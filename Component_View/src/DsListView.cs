﻿using System;
using System.Collections.Generic;
using System.Text;

using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Constant;
using Yamabuki.Design.GraphicsEx;
using Yamabuki.Design.Utility;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.View
{
    public class DsListView
        : DsSimpleComponent_1_0
    {
        private List<String> resultList = new List<String>();

        private DsTextureTextObject listImage;

        private Boolean dispose;

        public override String TypeName
        {
            get { return ""; }
        }

        internal String Result { get; private set; }

        public override void InitializeExecution()
        {
        }

        public override void FinalizeExecution()
        {
            var temp = "";
            var inputPort = this.InputPortCollection.GetPort(0);
            if (inputPort != null && inputPort.DataConnection != null)
            {
                var dataGuid = inputPort.DataConnection.DataStoreGuid;
                var dataStore = this.DataManager.GetDataStore(dataGuid);
                var dataList = dataStore.DataList;
                this.DataManager.RemoveDataStore(dataStore.Guid, this.DefinitionPath);
                if (dataList == null)
                {
                    return;
                }

                this.UpdateResultList(dataList);

                temp = this.GetResultString();
            }

            if (this.Result != temp)
            {
                this.Result = temp;
                this.dispose = true;
            }
        }

        public void UpdateResultList(TsDataList dataList)
        {
            if (dataList.Type == typeof(Int32))
            {
                this.resultList = ToStringList(dataList, new Func<Int32, String>(x => x.ToString()));
            }
            else if (dataList.Type == typeof(Int64))
            {
                this.resultList = ToStringList(dataList, new Func<Int64, String>(x => x.ToString()));
            }
            else if (dataList.Type == typeof(Single))
            {
                this.resultList = ToStringList(dataList, new Func<Single, String>(x => x.ToString()));
            }
            else if (dataList.Type == typeof(Double))
            {
                this.resultList = ToStringList(dataList, new Func<Double, String>(x => x.ToString()));
            }
            else if (dataList.Type == typeof(String))
            {
                this.resultList = ToStringList(dataList, new Func<String, String>(x => x));
            }
            else if (dataList.Type == typeof(Boolean))
            {
                this.resultList = ToStringList(dataList, new Func<Boolean, String>(x => x.ToString()));
            }
            else if (dataList.Type == typeof(DateTime))
            {
                this.resultList = ToStringList(dataList, new Func<DateTime, String>(x => x.ToString()));
            }
            else if (dataList.Type == typeof(Encoding))
            {
                this.resultList = ToStringList(dataList, new Func<Encoding, String>(x => x.WebName));
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            if (this.listImage != null)
            {
                this.listImage.Dispose();
            }
        }
        
        public override void SetSize(Int32 width, Int32 height)
        {
            base.SetSize(width, height);
            this.Redraw();
        }

        protected override void Initialize_1_0()
        {
        }

        protected override void DrawTypeName()
        {
            if (this.Result != null && this.dispose)
            {
                if (this.listImage != null)
                {
                    this.listImage.Dispose();
                }

                this.dispose = false;
                this.listImage = DsResource.GetInstance().CreateTexture(this.Result);
            }

            if (this.listImage != null)
            {
                this.listImage.X = this.X + DsComponentConstants.GridSize;
                this.listImage.Y = Math.Max(
                    this.Y + this.Height / 2 - this.listImage.ImageHeight / 2,
                    this.Y + DsComponentConstants.GridSize);
                this.listImage.Width = Math.Min(
                    this.listImage.ImageWidth,
                    this.Width - DsComponentConstants.GridSize * 2);
                this.listImage.Height = Math.Min(
                    this.listImage.ImageHeight,
                    this.Height - DsComponentConstants.GridSize * 2);
                this.listImage.Draw();
            }
        }

        protected override TsTask GetTask(TsDataStoreGuid inputDataStoreGuid)
        {
            return new TsTask_1_0_Ex(this.DefinitionPath, inputDataStoreGuid);
        }

        private static List<String> ToStringList<T>(TsDataList dataList, Func<T, String> func)
        {
            var result = new List<String>();

            var dataListT = dataList as TsDataListT<T>;
            var list = dataListT.ValueList;

            foreach (var value in list)
            {
                result.Add(func(value));
            }

            return result;
        }

        private void Redraw()
        {
            var temp = this.GetResultString();
            
            if (this.Result != temp)
            {
                this.Result = temp;
                if (this.listImage != null)
                {
                    this.listImage.Dispose();
                }

                this.listImage = null;
            }
        }

        private String GetResultString()
        {
            var sb = new StringBuilder();
            Int32 limit = 10;
            for (var i = 0; i < this.resultList.Count; i++)
            {
                sb.Append(this.resultList[i]);
                sb.Append("\r\n");

                if (i + 1 == limit)
                {
                    break;
                }
            }

            return sb.ToString();
        }

        private class TsTask_1_0_Ex
            : TsTask_1_0
        {
            public TsTask_1_0_Ex(String definitionPath, TsDataStoreGuid inputDataStoreGuid)
                : base(definitionPath, inputDataStoreGuid)
            {
            }

            public override void FinalizeExecution()
            {
            }

            protected override void CheckInputDataList(TsDataList inputDataList)
            {
            }

            protected override void ExecuteInternal(TsDataList inputDataList)
            {
            }
        }
    }
}
